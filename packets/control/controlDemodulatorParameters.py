from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,
                       Int32ul,
                       Int64ul, 
                       Int8ul,  
                       Int16ul)

    
    
class DemodulatorParametersPacket(BasePacket):
    """
        The class describes a Packet of constant parameters of the demodulator
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 9 # 8 from server, 9 to server
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 96
    
    result: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort, "0 - ok, else - err code from ResultCodes enum"]    
    
    carrierFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    clockFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    
    signalType: Annotated[int, Ge(0), Le(7), CTypes.Byte, """0 - BPSK, 
                          1 - QPSK, 
                          2 - OQPSK, 
                          3 - 8PSK, 
                          4 - 16QAM,
                          5 - 16APSK,
                          6 - 32APSK,
                          7 - DVB-S2"""]
    
    packingCode: Annotated[int, Ge(0), Le(22), CTypes.Byte, """0 - off, 
                          1 - intelsat, 
                          2 - Gray 1, 
                          3 - Gray 2, 
                          4 - Gray 3,
                          5 - 4D TCM,
                          6 - DVB S2,
                          7 - G2.57,
                          8 - G2.60,
                          9 - G2.70,
                          10 - G2.75,
                          11 - G2.85,
                          12 - G2.95,
                          13 - G3.15,
                          14 - G3.20,
                          15 - G3.82,
                          16 - BPSK 180 deg,
                          17 - BPSK 135 deg,
                          18 - G2.53,
                          19 - G2.54,
                          20 - G2.64,
                          21 - G2.72,
                          22 - G2.84
                          """]
    
    filterType: Annotated[int, Ge(0), Le(6), CTypes.Byte, """0 - IBS, 
                          1 - SRRC-0.3, 
                          2 - SRRC-0.35, 
                          3 - SRRC-0.4, 
                          4 - SRRC-0.5,
                          5 - NfNyq-0.4,
                          1 - SRRC-0.2,
                          1 - SRRC-0.25,
                          6 - Wide
                          """]
    
    PPLBand: Annotated[int, Ge(0), Le(3), CTypes.Byte, """0 - 1/400, 
                          1 - 1/800, 
                          2 - 1/1600, 
                          3 - 1/3200
                          """]
    
    # Data packaging
    
    clockSkip: Annotated[int, Ge(0), Le(255), CTypes.Byte, """Only for DEM500"""]
    
    bitsPacking: Annotated[int, Ge(0), Le(1), CTypes.Byte, """0 - LSB, 1 - MSB"""]
    bytesPacking: Annotated[int, Ge(0), Le(1), CTypes.Byte, """0 - LSB, 1 - MSB"""]
    
    isQPSKOffset: Annotated[int, Ge(0), Le(1), CTypes.Byte, """Only for DEM500"""]
    
    isAdditionalByte: Annotated[int, Ge(0), Le(1), CTypes.Byte, """Only for DEM500"""]
    
    isDataInvertion: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    # Decoder parameters
    
    innerDecoderType: Annotated[int, Ge(0), Le(15), CTypes.Byte, """0 - off, 
                          1 - Viterbi 1/2, 
                          2 - Viterbi 1/2 I, 
                          3 - Viterbi 1/2 x2,
                          4 - Viterbi 3/4,
                          5 - Viterbi 7/8,
                          6 - LDPC NASA,
                          7 - 4D 8PSK TCM 2.0,
                          8 - 4D 8PSK TCM 2.25,
                          9 - 4D 8PSK TCM 2.5,
                          10 - 4D 8PSK TCM 2.75,
                          11 - DSNG 8PSK 5/6,
                          12 - LDPC NASA X2,
                          13 - Manchester,
                          14 - Viterbi 3/4 x2,
                          15 - LDPC-NASA-14-15
                          """]
    
    outerDecoderType: Annotated[int, Ge(0), Le(4), CTypes.Byte, """0 - off, 
                          1 - RS-CCSDS I=4,
                          2 - RS-CCSDS I=8,
                          3 - RS-DVB(204, 188),
                          4 - RS-CCSDS I=5,
                          """]
    
    isSpecterInvertion: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isDiffDecoder: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isDescrambler: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    universalDescramblerType: Annotated[int, Ge(0), Le(3), CTypes.Byte, """0 - CCSDS, 
                          2 - 14-15 (before decoder),
                          3 - 14-15 (after decoder),
                          """, "only for DEM500M and LDPC NASA or LDPC NASE X2"]
    
    startStateUniversalDescrambler: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "see document for details"]
    
    isInvertor: Annotated[int, Ge(0), Le(1), CTypes.Byte, "Inversion after decoder"]
    
    isIQChannelsInversion: Annotated[int, Ge(0), Le(3), CTypes.Byte, """0 - no inversion, 
                          1 - I channel inversion,
                          2 - Q channel inversion,
                          3 - IQ channel inversion,
                          """]
    
    isIQShuffling: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isBitTurn: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isSycroWordInsert: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isCheckingBits: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isEnableFrameSync: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    # Frame synchronization parameters
    
    syncroWord: Annotated[int, Ge(0), Le(CTypes.UInt_64.max), CTypes.UInt_64]
    syncroMask: Annotated[int, Ge(0), Le(CTypes.UInt_64.max), CTypes.UInt_64]
    frameLength: Annotated[int, Ge(0), Le(CTypes.UInt_16.max), CTypes.UInt_16]
    searchThreshold: Annotated[int, Ge(0), Le(255), CTypes.Byte]
    lossThreshold: Annotated[int, Ge(0), Le(255), CTypes.Byte]
    
    # DVB-S2 parameters
    
    scramblerOffset: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    header: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    alignmentPackets: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    alignmentPacketsBytes: Annotated[int, Ge(393), Le(8191), CTypes.UInt_32]
    removePacketsWithErrors: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    DFLProcessing: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    UPLProcessing: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isSpecterInversion: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isFrequencyAutoTune: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isCarrierFrequencyTracking: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isClockFrequencyTracking: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isAdaptiveCorrector: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isAdaptiveCorrectorFix: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isClockFrequencyInversion: Annotated[int, Ge(0), Le(1), CTypes.Byte] # Инверсит ТЧ ?
    isLVDS: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isExternalReference: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isAmpPhaseCorrection: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    # Gain Control parameters
    
    isEnableAGC1: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isEnableAGC2: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    GC1Value: Annotated[int, Ge(0), Le(127), CTypes.Byte, """0...127 for DEM200 and DEM500, 
                         0...60 for DEM400, 
                         0...99 for DEM500M"""]
    GC2Value: Annotated[int, Ge(0), Le(65535), CTypes.UInt_16, """0...65535 for DEM200 and DEM500 and DEM500M, 
                         0...4095 for DEM500"""]
    
    preselectorFrequencyOffset: Annotated[int, Ge(-60), Le(60), CTypes.Int_32]
    AGC1Threshold: Annotated[int, Ge(0), Le(140), CTypes.UInt_16, """0...100 for DEM500 and DEM500M, 
                         0...140 for DEM200"""]
    
    reserved: Annotated[int, Ge(0), Le(255), CTypes.Byte] = 0 # do hard reboot if set 0xFF. Recomended before use
    
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "result" / Int32ul,
        "carrierFrequency" / Int32ul,
        "clockFrequency" / Int32ul,
        "signalType" / Int8ul,
        "packingCode" / Int8ul,
        "filterType" / Int8ul,
        "PPLBand" / Int8ul,
        "clockSkip" / Int8ul,
        "bitsPacking" / Int8ul,
        "bytesPacking" / Int8ul,
        "isQPSKOffset" / Int8ul,
        "isAdditionalByte" / Int8ul,
        "isDataInvertion" / Int8ul,
        "innerDecoderType" / Int8ul,
        "outerDecoderType" / Int8ul,
        "isSpecterInvertion" / Int8ul,
        "isDiffDecoder" / Int8ul,
        "isDescrambler" / Int8ul,
        "universalDescramblerType" / Int8ul,
        "startStateUniversalDescrambler" / Int32ul,
        "isInvertor" / Int8ul,
        "isIQChannelsInversion" / Int8ul,
        "isIQShuffling" / Int8ul,
        "isBitTurn" / Int8ul,
        "isSycroWordInsert" / Int8ul,
        "isCheckingBits" / Int8ul,
        "isEnableFrameSync" / Int8ul,
        "syncroWord" / Int64ul,
        "syncroMask" / Int64ul,
        "frameLength" / Int16ul,
        "searchThreshold" / Int8ul,
        "lossThreshold" / Int8ul,
        "scramblerOffset" / Int32ul,
        "header" / Int8ul,
        "alignmentPackets" / Int8ul,
        "alignmentPacketsBytes" / Int32ul,
        "removePacketsWithErrors" / Int8ul,
        "DFLProcessing" / Int8ul,
        "UPLProcessing" / Int8ul,
        "isSpecterInversion" / Int8ul,
        "isFrequencyAutoTune" / Int8ul,
        "isCarrierFrequencyTracking" / Int8ul,
        "isClockFrequencyTracking" / Int8ul,
        "isAdaptiveCorrector" / Int8ul,
        "isAdaptiveCorrectorFix" / Int8ul,
        "isClockFrequencyInversion" / Int8ul,
        "isLVDS" / Int8ul,
        "isExternalReference" / Int8ul,
        "isAmpPhaseCorrection" / Int8ul,
        "isEnableAGC1" / Int8ul,
        "isEnableAGC2" / Int8ul,
        "GC1Value" / Int8ul,
        "GC2Value" / Int16ul,
        "preselectorFrequencyOffset" / Int32sl,
        "AGC1Threshold" / Int16ul,
        "reserved" / Int8ul
        )
    