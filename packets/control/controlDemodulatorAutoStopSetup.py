from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Int32ul,
                       Int16ul)

    
    
class DemodulatorAutoStopSetupPacket(BasePacket):
    """
        This class describes a packet to set the demodulator to auto-off when the connection is closed
        
        Note: the command cannot be sent while the demodulator is running.
        Note: If the client has not configured the demodulator auto-stop settings and
        communication with the server was interrupted while data was being written to disk, the server will
        will automatically stop the demodulator and file writing.
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 73
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 8
    
    isAutoStopMode: Annotated[bool, Ge(0), Le(1), CTypes.UInt_32]
    autoStopDelay: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "In seconds. Only if autoStopMode enabled"]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "isAutoStopMode" / Int32ul,
        "autoStopDelay" / Int32ul
        )
    