from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Int64ul,
                       Int16ul)

    
    
class ServerGetCurrentFileRequestPacket(BasePacket):
    """
        This class describes a packet for requesting information about the path of the current file
        
        This command allows you to get the full path to a data file that is being (or was written) on the server. 
        This command is relevant if the option to write data on the server is enabled in the demodulator start command.
        server in the demodulator start command. The command must be sent after sending the
        demodulator start command in order to receive the current file being written.
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 61
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 0
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul
        )
    