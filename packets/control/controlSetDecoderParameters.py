from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,
                       Int32ul,
                       Int64ul, 
                       Int8ul,  
                       Int16ul)

    
    
class SetDecoderParametersPacket(BasePacket):
    """
        The class describes a packet of parameters 
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 14
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 34
    
    # Decoder parameters
    
    innerDecoderType: Annotated[int, Ge(0), Le(15), CTypes.Byte, """0 - off, 
                          1 - Viterbi 1/2, 
                          2 - Viterbi 1/2 I, 
                          3 - Viterbi 1/2 x2,
                          4 - Viterbi 3/4,
                          5 - Viterbi 7/8,
                          6 - LDPC NASA,
                          7 - 4D 8PSK TCM 2.0,
                          8 - 4D 8PSK TCM 2.25,
                          9 - 4D 8PSK TCM 2.5,
                          10 - 4D 8PSK TCM 2.75,
                          11 - DSNG 8PSK 5/6,
                          12 - LDPC NASA X2,
                          13 - Manchester,
                          14 - Viterbi 3/4 x2,
                          15 - LDPC-NASA-14-15
                          """]
    
    outerDecoderType: Annotated[int, Ge(0), Le(4), CTypes.Byte, """0 - off, 
                          1 - RS-CCSDS I=4,
                          2 - RS-CCSDS I=8,
                          3 - RS-DVB(204, 188),
                          4 - RS-CCSDS I=5,
                          """]
    
    isSpecterInvertion: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isDiffDecoder: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isDescrambler: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    universalDescramblerType: Annotated[int, Ge(0), Le(3), CTypes.Byte, """0 - CCSDS, 
                          2 - 14-15 (before decoder),
                          3 - 14-15 (after decoder),
                          """, "only for DEM500M and LDPC NASA or LDPC NASE X2"]
    
    startStateUniversalDescrambler: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "see document for details"]
    
    isInvertor: Annotated[int, Ge(0), Le(1), CTypes.Byte, "Inversion after decoder"]
    
    isIQChannelsInversion: Annotated[int, Ge(0), Le(3), CTypes.Byte, """0 - no inversion, 
                          1 - I channel inversion,
                          2 - Q channel inversion,
                          3 - IQ channel inversion,
                          """]
    
    isIQShuffling: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isBitTurn: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isSycroWordInsert: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isCheckingBits: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    isEnableFrameSync: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    # Frame synchronization parameters
    
    syncroWord: Annotated[int, Ge(0), Le(CTypes.UInt_64.max), CTypes.UInt_64]
    syncroMask: Annotated[int, Ge(0), Le(CTypes.UInt_64.max), CTypes.UInt_64]
    frameLength: Annotated[int, Ge(0), Le(CTypes.UInt_16.max), CTypes.UInt_16]
    searchThreshold: Annotated[int, Ge(0), Le(255), CTypes.Byte]
    lossThreshold: Annotated[int, Ge(0), Le(255), CTypes.Byte]
    
    # DVB-S2 parameters
    
    scramblerOffset: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    header: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    alignmentPackets: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    alignmentPacketsBytes: Annotated[int, Ge(393), Le(8191), CTypes.UInt_32]
    removePacketsWithErrors: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    DFLProcessing: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    UPLProcessing: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isSpecterInversion: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "innerDecoderType" / Int8ul,
        "outerDecoderType" / Int8ul,
        "isSpecterInvertion" / Int8ul,
        "isDiffDecoder" / Int8ul,
        "isDescrambler" / Int8ul,
        "universalDescramblerType" / Int8ul,
        "startStateUniversalDescrambler" / Int32ul,
        "isInvertor" / Int8ul,
        "isIQChannelsInversion" / Int8ul,
        "isIQShuffling" / Int8ul,
        "isBitTurn" / Int8ul,
        "isSycroWordInsert" / Int8ul,
        "isCheckingBits" / Int8ul,
        "isEnableFrameSync" / Int8ul,
        "syncroWord" / Int64ul,
        "syncroMask" / Int64ul,
        "frameLength" / Int16ul,
        "searchThreshold" / Int8ul,
        "lossThreshold" / Int8ul,
        "scramblerOffset" / Int32ul,
        "header" / Int8ul,
        "alignmentPackets" / Int8ul,
        "alignmentPacketsBytes" / Int32ul,
        "removePacketsWithErrors" / Int8ul,
        "DFLProcessing" / Int8ul,
        "UPLProcessing" / Int8ul,
        "isSpecterInversion" / Int8ul
        )
    