from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl, 
                       Int8ul, 
                       Int16ul)

    
    
class SetFilterPacket(BasePacket):
    """
        The class describes a packet for managing filters
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 15
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 4
    
    filterType: Annotated[int, Ge(0), Le(6), CTypes.Byte, """0 - IBS, 
                          1 - SRRC-0.3, 
                          2 - SRRC-0.35, 
                          3 - SRRC-0.4, 
                          4 - SRRC-0.5,
                          5 - NfNyq-0.4,
                          1 - SRRC-0.2,
                          1 - SRRC-0.25,
                          6 - Wide
                          """]
    
    reserved1: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    reserved2: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "filterType" / Int8ul,
        "reserved1" / Int8ul,
        "reserved2" / Int8ul
        )
    