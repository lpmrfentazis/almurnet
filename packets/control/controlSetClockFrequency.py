from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl, 
                       Int32ul, 
                       Int16ul)

    
    
class SetClockFrequencyPacket(BasePacket):
    """
        The class describes a packet that specifies the clock frequency
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 10
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 8
    
    clockFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "clockFrequency" / Int32ul)
    