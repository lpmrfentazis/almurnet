from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl, 
                       Int16ul,
                       Int8sl,
                       GreedyRange)

    
    
class LoadUserConfigurationPacket(BasePacket):
    """
        this class describes a packet that allows you to load a custom configuration from the ini file received from the Demodulator client app
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 39
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort, "file size in bytes"]
    
    data: Annotated[bytes, MinLen(dataLength), MaxLen(dataLength), list[CTypes.Byte]]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "data" / GreedyRange(Int8sl)
        )
    