from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl, 
                       PaddedString,
                       Int16ul)

    
    
class ServerGetCurrentFilePacket(BasePacket):
    """
        This class describes a packet information about the path of the current file
    """
    _filePathLen: int = 1024
    
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 62
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 1024
    
    filePath: Annotated[str, MinLen(_filePathLen), MaxLen(_filePathLen)]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "filePath" / PaddedString(length=_filePathLen, encoding="utf8")
    )
    