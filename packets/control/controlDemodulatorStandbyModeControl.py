from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl, 
                       Int16ul,
                       Byte)

    
    
class DemodulatorStandbyModeControlPacket(BasePacket):
    """
        This class describes a packet that allows you to enable or disable standby mode
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 44
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 1
    
    isStandby: Annotated[bool, Ge(0), Le(1), CTypes.Byte]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "isStandby" / Byte
        )
    