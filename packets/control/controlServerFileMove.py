from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       PaddedString,
                       Int16ul)

    
    
class ServerFileMove(BasePacket):
    """
        This class describes a packet for requesting move or rename file on server
        
    """
    _filePathLen: int = 1024
    
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 71
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 2 * _filePathLen
    
    currentFilePath: Annotated[str, MinLen(_filePathLen), MaxLen(_filePathLen)]
    newFilePath: Annotated[str, MinLen(_filePathLen), MaxLen(_filePathLen)]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "currentFilePath" / PaddedString(length=_filePathLen, encoding="utf8"),
        "newFilePath" / PaddedString(length=_filePathLen, encoding="utf8")
        )
    