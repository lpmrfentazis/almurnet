from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl, 
                       Int32ul, 
                       Int16ul,
                       Byte)

    
    
class SelectDemodulatorPacket(BasePacket):
    """
        The class describes a packet for selecting a demodulator on the server
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 3
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 4
    demodulatorID: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "demodulatorID" / Int32ul)
    