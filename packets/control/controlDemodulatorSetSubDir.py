from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MaxLen, MinLen
from construct import (Struct, 
                       Int32sl,  
                       Int16ul,
                       PaddedString)

    
    
class DemodulatorSetSubDirPacket(BasePacket):
    """
        The class describes a packet for set sub dir for server data
    """
    _dirNameSize: int = 256
    
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 80
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = _dirNameSize
    
    subDirName: Annotated[str, MinLen(_dirNameSize), MaxLen(_dirNameSize), "An empty name means that the output data will be written to the main directory"]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "filePath" / PaddedString(length=_dirNameSize, encoding="utf8"))
    