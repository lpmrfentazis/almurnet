from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Int64ul,
                       Int16ul)

    
    
class ServerVersionPacket(BasePacket):
    """
        This class describes a packet server version information
        
        The server version consists of four
            numbers separated by a dot: Version1.Version2.Version3.Version4.
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 59
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 4
    
    version1: Annotated[int, Ge(CTypes.Byte.min), Le(CTypes.Byte.max), CTypes.Byte]
    version2: Annotated[int, Ge(CTypes.Byte.min), Le(CTypes.Byte.max), CTypes.Byte]
    version3: Annotated[int, Ge(CTypes.Byte.min), Le(CTypes.Byte.max), CTypes.Byte]
    version4: Annotated[int, Ge(CTypes.Byte.min), Le(CTypes.Byte.max), CTypes.Byte]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul
        )
    