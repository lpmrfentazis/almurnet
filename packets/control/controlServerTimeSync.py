from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Int64ul,
                       Int16ul)

    
    
class ServerTimeSyncPacket(BasePacket):
    """
        This class describes a packet for requesting information about a file written on the server by its file ID
        
        This command allows you to set the time on the server. The time is set as local time, i.e..
            the time zone of the server is taken into account. 
            
        Requires administrator privileges
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 57
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 8
    
    time: Annotated[int, Ge(CTypes.UInt_64.min), Le(CTypes.UInt_64.max), CTypes.UInt_64, "Unix timestamp in milliseconds"]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "time" / Int64ul
        )
    