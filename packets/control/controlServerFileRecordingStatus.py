from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Byte,
                       Int16ul)

    
    
class ServerFileRecordingStatusRequesPacket(BasePacket):
    """
        This class describes a packet information about current file recording status
        
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 55
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 1
    
    isFileRecording: Annotated[bool, Ge(0), Le(1), CTypes.Byte]
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "mode" / Byte
        )
    