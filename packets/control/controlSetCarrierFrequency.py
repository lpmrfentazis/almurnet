from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl, 
                       Int32ul, 
                       Int16ul,
                       Int16sl)

    
    
class SetCarrierFrequencyPacket(BasePacket):
    """
        The class describes a packet that specifies the carrier frequency
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 10
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 8
    
    carrierFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    preselectorFrequencyOffset: Annotated[int, Ge(-60), Le(60), CTypes.Int_16, "Only for DEM200. n % 5 shoud be 0"]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "carrierFrequency" / Int32ul,
        "preselectorFrequencyOffset", Int16sl)
    