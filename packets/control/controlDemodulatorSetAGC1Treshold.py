from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,
                       Int32ul,  
                       Int16ul)

    
    
class DemodulatorSetAGC1ThesholdPacket(BasePacket):
    """
        The class describes a packet for set AGC1 threshold
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 77
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 4
    
    AGC1Treshold: Annotated[int, Ge(CTypes.UInt_32.min), Le(CTypes.UInt_32.max), CTypes.UInt_32, """1...100 for DEM500 and DEM500M, 1...140 for DEM200"""]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "AGC1Treshold" / Int32ul
        )
    