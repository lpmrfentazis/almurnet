from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,  
                       Int16ul)

    
    
class DemodulatorInfoRequestPacket(BasePacket):
    """
        The class describes a packet for requesting the constant parameters of the demodulator
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 7
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 0
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul)
    