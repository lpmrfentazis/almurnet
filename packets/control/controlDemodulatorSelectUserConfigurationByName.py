from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl, 
                       Int16ul,
                       Int8sl,
                       GreedyRange)

    
    
class LoadUserConfigurationPacket(BasePacket):
    """
        this class describes a packet that allows you to select a custom configuration from user_config_list.xml
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 46
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort, "Name size without \\0 symbol"]
    
    configurationName: Annotated[bytes, MinLen(dataLength), MaxLen(dataLength), list[CTypes.Byte]]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "configurationName" / GreedyRange(Int8sl)
        )
    