from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl, 
                       Int8ul, 
                       Int16ul)
    
    
class SetGCParametersPacket(BasePacket):
    """
        The class describes the packet for controlling the gain control
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 16
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 4
    
    isEnableAGC: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    GCValue: Annotated[int, Ge(0), Le(127), CTypes.Byte, """0...127 for DEM200 and DEM500, 
                         0...60 for DEM400, 
                         0...99 for DEM500M"""]
    reserved0: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "isEnableAGC" / Int8ul,
        "GCValue" / Int8ul,
        "reserved0" / Int8ul
        )
    
class ControlSetGC1ParametersPacket(SetGCParametersPacket):
    """
        The class describes the packet for controlling the gain control 1
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 16
    
class ControlSetGC2ParametersPacket(SetGCParametersPacket):
    """
        The class describes the packet for controlling the gain control 2
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 17