from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Byte,
                       Int16ul)

    
    
class ServerDataWriteModeSelect(BasePacket):
    """
        This class describes a packet for requesting server version information
        
        This command allows you to set the data recording mode on the server when the 
        "Signal Search" option in the demodulator start command is enabled 
        When the signal search is enabled and synchronization on the
        carrier synchronization is lost, the signal search algorithm is started, 
        which leads to stopping of writing to the data file (as well as the log).
        to the data file (as well as the telemetry log file) on the server side. 
        
        When carrier synchronization is restored, a new data file is created (as well as the telemetry log file). 
        However, the client may need the mode of writing data to a single data file (as well as the telemetry log file).
        data file (as well as the telemetry log file) from start to stop of the demodulator, and the telemetry log file is not suspended.
        telemetry log file does not pause recording even when a signal is being searched.
        This command must be sent before starting the demodulator, otherwise the server will respond with an error.
        error.
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 60
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 1
    
    mode: Annotated[bool, Ge(0), Le(1), CTypes.Byte, "0 (default) - separated files, 1 - into one file"] = 1
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "mode" / Byte
        )
    