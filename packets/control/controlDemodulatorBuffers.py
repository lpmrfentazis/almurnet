from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Int64ul,
                       Int16ul)

    
    
class DemodulatorBuffers(BasePacket):
    """
        This class describes a packet information about a upper and lower buffers
        
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 64
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 16
    
    lowerBuffers: Annotated[int, Ge(CTypes.UInt_64.min), Le(CTypes.UInt_64.max), CTypes.UInt_64, "In bytes"]
    upperBuffers: Annotated[int, Ge(CTypes.UInt_64.min), Le(CTypes.UInt_64.max), CTypes.UInt_64, "In bytes"]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "lowerBuffers" / Int64ul,
        "upperBuffers" / Int64ul
        )
    