from packets.confirmPacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,  
                       Int32ul, 
                       Int64ul,
                       Int16ul,
                       PaddedString)

    
    
class ServerWritedFileInfoPacket(BasePacket):
    """
        This class describes a packet about a file written on the server
        
        When the demodulator is in active mode, various errors may occur.
        various errors may occur, such as loss of synchronization. If the option
        "Write data to disk" option was activated when starting the demodulator, then when synchronization is lost, file recording on the
        server is stopped, and when synchronization is restored, it is resumed. This command allows you to
        get information about all recorded files, starting from the moment of sending the command to start the demodulator.
        demodulator start command. The index of the last recorded file on the server is transmitted in the telemetry packet.
    """
    _filePathLen: int = 1024
    
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 19
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 1036
    
    result: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort, "0 - ok, else - err code from ResultCodes enum"]
    
    fileSize: Annotated[int, Ge(CTypes.UInt_64.min), Le(CTypes.UInt_64.max), CTypes.UInt_64, "In bytes"]
    filePath: Annotated[str, MinLen(_filePathLen), MaxLen(_filePathLen)]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "result" / Int32ul,
        "fileSize" / Int64ul,
        "filePath" / PaddedString(length=_filePathLen, encoding="utf8")
        )
    