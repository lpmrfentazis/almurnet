from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl, 
                       Int8ul, 
                       Int16ul)

    
    
class ActivateDemodulatorPacket(BasePacket):
    """
        The class describes the packet activating demodulator
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 20
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 4
    
    isDiskDataWrite: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isNetDataTransmission: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    isSignalSearch: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    dataSource: Annotated[int, Ge(0), Le(CTypes.Byte.max), CTypes.Byte, """0xFC - 1xIQ signed, 
                         0xFD - 2xIQ signed,
                         0xEC - 1xIQ unsigned,
                         0xED - xxIQ unsigned,
                         0xFF - specter""", "any other - Dem/Dec"] = 0
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "isDiskDataWrite" / Int8ul,
        "isNetDataTransmission" / Int8ul,
        "isSignalSearch" / Int8ul,
        "dataSource" / Int8ul
        )
    