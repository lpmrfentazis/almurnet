from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl, 
                       Int8ul, 
                       Int16ul)

    
    
class DeactivateDemodulatorPacket(BasePacket):
    """
        The class describes the packet activating demodulator
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 36
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 1
    
    isDeactivate: Annotated[int, Ge(0), Le(1), CTypes.Byte]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "isDeactivate" / Int8ul
        )
    