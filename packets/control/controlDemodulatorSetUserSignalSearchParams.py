from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,
                       Int32ul,  
                       Int16ul,
                       Byte)

    
    
class DemodulatorSetUserSignalSearchParamsPacket(BasePacket):
    """
        The class describes a packet for set custom parameters for signal search
        
        This command allows you to set the left and right offsets from the nominal carrier frequency, 
        within which the carrier search will be performed (if the carrier search algorithm has been enabled).
        within which the carrier search will be performed (if the carrier search algorithm was enabled when the demodulator was started).
        when the demodulator was started). For example, with a nominal carrier frequency of 1200 MHz and a left offset of
        100 kHz, and a right offset of 200 kHz, the carrier search range would be from 1199.9 MHz to 1200.2 MHz.
        This command is useful when you want to override the default parameters and thereby
        prevent the capture of a "false" carrier.
        
        The command must be sent before starting the demodulator.
        
        Note: this command can be sent after the demodulator standby mode is turned off.
            demodulator standby mode.
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 83
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 5
    
    leftOffset: Annotated[int, Ge(0), Le(200_000), CTypes.UShort, "In Hz"]
    rightOffset: Annotated[int, Ge(0), Le(200_000), CTypes.UShort, "In Hz"]
    
    isUserParamsEnable: Annotated[bool, Ge(0), Le(1), CTypes.Byte] = 1
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "leftOffset" / Int32ul,
        "rightOffset" / Int32ul,
        "isUserParamsEnable" / Byte
        )
    