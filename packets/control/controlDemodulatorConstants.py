from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,
                       Int32ul,  
                       Int16ul)

    
    
class DemodulatorConstantsPacket(BasePacket):
    """
        The class describes a packet of constant parameters of the demodulator
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 6
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 40
    
    result: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort, "0 - ok, else - err code from ResultCodes enum"]    
    demodulatorSerialNumber: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    demodulatorVersion: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    PCINumber: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    
    converterSerialNumber: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    converterVersion: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    
    minCarrierFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    maxCarrierFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    
    minClockFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    maxClockFrequency: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "result" / Int32ul,
        "demodulatorSerialNumber" / Int32ul,
        "demodulatorVersion" / Int32ul,
        "PCINumber" / Int32ul,
        "converterSerialNumber" / Int32ul,
        "converterVersion" / Int32ul,
        "minCarrierFrequency" / Int32ul,
        "maxCarrierFrequency" / Int32ul,
        "minClockFrequency" / Int32ul,
        "maxClockFrequency" / Int32ul,
        )
    