from dataclasses import dataclass
# A bucket of shit to convenient support byte structures

@dataclass
class Int_8:
    size: int = 1
    min: int = -2**(size * 8 - 1)
    max: int = 2**(size * 8 - 1) - 1

@dataclass
class UInt_8:
    size: int = 1
    min: int = 0
    max: int = 2**(size * 8) - 1

@dataclass
class Int_16:
    size: int = 2
    min: int = -2**(size * 8 - 1)
    max: int = 2**(size * 8 - 1) - 1
    
@dataclass
class UInt_16:
    size: int = 2
    min: int = 0
    max: int = 2**(size * 8) - 1
    
@dataclass
class Int_32:
    size: int = 4
    min: int = -2**(size * 8 - 1)
    max: int = 2**(size * 8 - 1) - 1
    
@dataclass
class UInt_32:
    size: int = 4
    min: int = 0
    max: int = 2**(size * 8) - 1
    
@dataclass
class Int_64:
    size: int = 8
    min: int = -2**(size * 8 - 1)
    max: int = 2**(size * 8 -1) - 1
    
@dataclass
class UInt_64:
    size: int = 8
    min: int = 0
    max: int = 2**(size * 8) - 1


# Aliases

Char = Int_8
Byte = UInt_8
UChar = UInt_8
SChar = Int_8
Short = Int_16
UShort = UInt_16
Long = Int_32
ULong = UInt_32
LongLong = Int_64
ULongLong = UInt_64