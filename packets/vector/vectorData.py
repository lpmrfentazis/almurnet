from __future__ import annotations
from packets.basePacket import BaseStruct
from annotated_types import Annotated, MinLen, MaxLen
from packets.CTypes import CTypes
from construct import (Struct, 
                       Int8sl,
                       GreedyRange)


class BasePacket(BaseStruct):
    """
       This class describes a packet of vector diagram data
    """
    _bufferLength: int = 4096
    bufferIQ: Annotated[bytes, MinLen(_bufferLength), MaxLen(_bufferLength), list[CTypes.Byte]]
    
    _format: Struct = Struct(
        "bufferIQ" / GreedyRange(Int8sl)
    )