from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,  
                       Int16ul)

    
    
class VectorDataPacket(BasePacket):
    """
        The class describes a packet for requesting vector diagram data
        
        The Vector Chart port is used to obtain IQ samples and build a constellation.
        Note: the format of the samples is QI, not IQ. This must be taken into account when building
        constellation.
        Command to get the actual buffer of IQ samples
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 38
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 0
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul)
    