from .basePacket import *
from .telemetry import *
from .confirmPacket import *
from .provider import *
from .telemetry import *

from .vector import *
from .resultCodes import *

"""
Based on Demodulator_TCP_Protocol_v46
"""

TelemetryInitPacket = TelemetryRequestPacket(mode=1)
TelemetryPingPacket = BasePacket(packetID=48, dataLength=0)
#TelemetryByRequestModePacket = Tele