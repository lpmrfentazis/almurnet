import enum


class ResultCodes(enum.IntEnum):
    OK: int = 0
    DEMODULATOR_ID_NOT_SET: int = 1
    DEMODULATOR_ID_ALREADY_SET: int = 2        # Warning
    DEMODULATOR_ID_WRONG: int = 3         
    DEMODULATOR_LOAD_ERROR: int = 4           # look demodulator log file
    DEMODULATOR_FILE_NOT_FOUND: int = 5        # look demodulator log file
    DEMODULATOR_LICENSE_NOT_FOUND: int = 6
    DEMODULATOR_WRONG_LICENSE: int = 7
    AGC1_ALREADY_ENABLED: int = 8
    AGC2_ALREADY_ENABLED: int = 9
    GC1_WRONG_VALUE: int = 10
    GC2_WRONG_VALUE: int = 11
    INCOMPATIBILITY_SIGNAL_AND_CODE: int = 12
    INCOMPATIBILITY_INNER_AND_OUTER_DECODER: int = 13
    SIGNAL_WRONG_VALUE: int = 14
    CODE_WRONG_VALUE: int = 15
    FILTER_WRONG_VALUE: int = 16
    PPL_WRONG_VALUE: int = 17
    INNER_DECODER_WRONG_VALUE: int = 18
    OUTER_DECODER_WRONG_VALUE: int = 19
    IQ_INVERSION_WRONG_VALUE: int = 20
    RS_CCSDS_I4_WRONG_FRAME: int = 21           # Frame length and out frame length must be 1024
    RS_CCSDS_I8_WRONG_FRAME: int = 22           # Frame length and out frame length must be 2044
    RS_DVB_204_188_WRONG_FRAME: int = 23        # Frame length and out frame length must be 1024
    COMMANT_NOT_SUPPORTED: int = 24             # Command execution is not possible on the specified port
    FAILED_READ_REGISTER_VALUE: int = 25
    FAILED_WRITE_REGISTER_VALUE: int = 26
    FILE_SIZE_WRONG_VALUE: int = 27             # An error occurs when loading a custom configuration 
                                                #which is transferred from the client server if the file size is specified as zero.
    FAILED_CREATE_TEMP_FILE_ON_SERVER: int = 28
    CARRIER_FREQUENCY_WRONG_VALUE: int = 29
    CLOCK_FREQUENCT_WRONG_VALUE: int = 30
    CONFIGURATION_ID_NOT_FOUND: int = 31
    CONFIGURATION_NAME_NOT_FOUND: int = 32
    DEMODULATOR_BUSY: int = 33
    FAILED_CREATE_DATA_FILE_ON_SERVER: int = 34
    DEMODULATOR_ALREADY_ACTIVE: int = 35
    DEMODULATOR_STILL_ACTIVE: int = 36
    IMPOSIBLE_EXECUTE_COMMAND_DEMODULATOR_ACTIVE: int = 37
    LDPC_NASA_X2_WRONG_FRAME: int = 38           # Frame length and out frame length must be 2048
    FILE_WITH_ID_NOT_FOUND: int = 39
    SERVER_SET_TIME_FAILED: int = 40
    SERVER_TELEMETRY_LOGFILE_CREATE_FAIL: int = 41
    SERVER_TELEMETRY_DIRECTORY_CREATE_FAIL: int = 42
    IQ_CHANNELS_INVERSION_WRONG_VALUE: int = 43
    RS_CCSDS_I5_WRONG_FRAME: int = 44           # Frame length and out frame length must be 1279
    DATA_FILE_NOT_FOUND: int = 45
    SERVER_FILE_RENAME_FAILED: int = 46
    PRESELECTOR_OFFSET_WRONG_VALUE: int = 47
    AGC1_TRESHOLD_WRONG_VALUE: int = 48
    SERVER_CREATE_DIRECTORY_FAIL: int = 49
    LEFT_RIGHT_FREQUENCY_OFFSET_WRONG_VALUE: int = 50
    IMPOSIBLE_EXECUTE_COMMAND_DEMODULATOR_STANDBY: int = 51
    
    # Telemetry errors
    TEMP_STORAGE_FULL: int = 100
    SERVER_FILE_WRITE_ERROR_BUFFER_FULL: int = 101
    SERVER_FILE_WRITE_ERROR: int = 102
    PCIE_BUS_OVERFLOW: int = 103
    SYNC_LOSS: int = 104
    INTERRUPTION_LOSS: int = 105
    DEMODULATOR_OVERGEATING: int = 106
    UNKNOWN: int = 666