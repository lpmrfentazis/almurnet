from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Array, 
                       Byte,
                       Int8sl,
                       Int32sl, 
                       Int32ul, 
                       Int16ul, 
                       Int16sl, 
                       Int64ul)

class TelemetryPacket(BasePacket):
    """
        Telemetry packet that is sent by the demodulator server
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 37
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 282
    
    isCarrierFreqSync: Annotated[bool, Ge(0), Le(1), CTypes.Byte] = 0
    isClockFreqSync: Annotated[bool, Ge(0), Le(1), CTypes.Byte] = 0
    isInDecoderSync: Annotated[int, Ge(0), Le(2), CTypes.Byte, "0 - decoder off, 1 - no sync, 2 - sync"] = 0
    isOutDecoderSync: Annotated[int, Ge(0), Le(2), CTypes.Byte, "0 - decoder off, 1 - no sync, 2 - sync"] = 0
    isFrameSync: Annotated[int, Ge(0), Le(2), CTypes.Byte, "0 - frame sync off, 1 - no sync, 2 - sync"] = 0
    isExternalSupportSync: Annotated[int, Ge(0), Le(2), CTypes.Byte, "0 - external support off, 1 - no sync, 2 - sync", "only DEM500M"] = 0
    
    carrierFreq: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"] = 0
    clockFreq: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "in Hz"] = 0
    
    uncorrectedBlocks: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32] = 0
    
    isAGC1: Annotated[bool, Ge(0), Le(1), CTypes.Byte] = 0
    isAGC2: Annotated[bool, Ge(0), Le(1), CTypes.Byte] = 0
    
    levelGC1: Annotated[int, Ge(0), Le(CTypes.Int_32.max), 
                        CTypes.Int_32, "for DEM200 < 127, DEM400 < 60, DEM500 < 127, DEM500M < 99"] = 0
    levelGC2: Annotated[int, Ge(0), Le(CTypes.Int_32.max), 
                        CTypes.Int_32, "for DEM200 < 65535, DEM400 < 65535, DEM500 < 4095, DEM500M < 65535"] = 0
    
    level1: Annotated[int, Ge(CTypes.Int_32.min), Le(CTypes.Int_32.max), CTypes.Int_32, "Dbm * 10"] = 0
    levelOut: Annotated[int, Ge(CTypes.Int_32.min), Le(CTypes.Int_32.max), CTypes.Int_32, "Db * 10"] = 0
    level2: Annotated[int, Ge(CTypes.Int_32.min), Le(CTypes.Int_32.max), CTypes.Int_32, "Dbm * 10"] = 0
    
    snr: Annotated[int, Ge(CTypes.Int_32.min), Le(CTypes.Int_32.max), CTypes.Int_32, "signal/noise, Db * 10"] = 0
    EbNo: Annotated[int, Ge(CTypes.Int_32.min), Le(CTypes.Int_32.max), CTypes.Int_32, "Db * 10"] = 0
    BER: Annotated[int, Ge(0), Le(CTypes.ULongLong.max), CTypes.ULongLong, "BER x 100000000"] = 0
    
    demodulatorTemp: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "deg C * 100"] = 0
    decoderTemp: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "deg C * 100", "only DEM400"] = 0
    interfaceTemp: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "deg C * 100", "only DEM400"] = 0
    ADCTemp: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "deg C * 100", "only DEM400"] = 0
    
    isochronousFIFOLevel: Annotated[int, Ge(CTypes.Short.min), Le(CTypes.Short.max), CTypes.Short] = 0
    coreVoltage: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "mV"] = 0
    peripheralsVoltage: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "mV"] = 0
    
    diskWriteSpeed: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "bytes/s"] = 0
    
    lowerLevelBuffFilling: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, 
                                     "amount of raw  information in  lower buffers  relative to their  total volume",
                                     "hundredths  fractions of a percent", "only if enable disk write data"] = 0
    
    upperLevelBuffFilling: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, 
                                     "amount of raw  information in  upper buffers  relative to their  total volume",
                                     "hundredths  fractions of a percent", "only if enable disk write data"] = 0
    
    errorCode: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32, "error codes from item 9, 0 if ok"] = 0
    timestamp: Annotated[int, Ge(0), Le(CTypes.ULongLong.max), CTypes.ULongLong, "unix timestamp in milliseconds"] = 0
    
    equalizerRe: Annotated[list[ Annotated[int, Ge(CTypes.Char.min), Le(CTypes.Char.max), CTypes.Char] ], MinLen(13), MaxLen(13)] = [0,] * 13
    equalizerIm: Annotated[list[ Annotated[int, Ge(CTypes.Char.min), Le(CTypes.Char.max), CTypes.Char] ], MinLen(13), MaxLen(13)] = [0,] * 13
    
    lastFileID: Annotated[int, Ge(-1), Le(CTypes.Int_32.max), CTypes.Int_32, 
                          "only when write data to disk", 
                          "-1 when no files have been recorded yet on the server"] = -1
    
    currentDiskDataSize: Annotated[int, Ge(0), Le(CTypes.ULongLong.max), CTypes.ULongLong,
                          "Only when write data to disk"] = 0
    
    BERUW: Annotated[int, Ge(0), Le(CTypes.ULongLong.max), CTypes.ULongLong.max, 
                     "only when frameSync on",
                     "BER UW * 100000000"] = 0
    
    isOverflowIsochronousFIFO: Annotated[bool, Ge(0), Le(1), CTypes.Byte] = 0
    
    # DVB-S2(X)
    isMODCOD: Annotated[bool, Ge(0), Le(1), CTypes.Byte, "Before reading out  MODCOD table it is necessary to check  this field"] = 0
    MODCOD: Annotated[ list[ Annotated[int, Ge(0), Le(1000), CTypes.UShort] ], MinLen(128), MaxLen(128), 
                      "Tenths of a percent  of MODCOD values"] = [0,]*128

    _format: Struct = Struct(
            "startMark" / Int32sl,
            "packetID" / Int16ul,
            "dataLength" / Int16ul,
            "isCarrierFreqSync" / Byte,
            "isClockFreqSync" / Byte,
            "isInDecoderSync" / Byte,
            "isOutDecoderSync" / Byte,
            "isFrameSync" / Byte,
            "isExternalSupportSync" / Byte,
            
            "carrierFreq" / Int32ul,
            "clockFreq" / Int32ul,
            
            "uncorrectedBlocks" / Int32ul,
            
            "isAGC1" / Byte,
            "isAGC2" / Byte,
            
            "levelGC1" / Int32sl,
            "levelGC2" / Int32sl,
            
            "level1" / Int32sl,
            "levelOut" / Int32sl,
            "level2" / Int32sl,
            
            "snr" / Int32sl,
            "EbNo" / Int32sl,
            "BER" / Int64ul,
            
            "demodulatorTemp" / Int32ul,
            "decoderTemp" / Int32ul,
            "interfaceTemp" / Int32ul,
            "ADCTemp" / Int32ul,
            
            "isochronousFIFOLevel" / Int16sl,
            "coreVoltage" / Int32ul,
            "peripheralsVoltage" / Int32ul,
            
            "diskWriteSpeed" / Int32ul,
            
            "lowerLevelBuffFilling" / Int32ul,
            "upperLevelBuffFilling" / Int32ul,
            
            "errorCode" / Int32ul,
            "timestamp" / Int32ul,
            
            "equalizerRe" / Array(13, Int8sl),
            "equalizerIm" / Array(13, Int8sl),
            
            "lastFileID" / Int32sl,
            
            "currentDiskDataSize" / Int64ul,
            
            "BERUW" / Int64ul,
            
            "isOverflowIsochronousFIFO" / Byte,
            
            "isMODCOD" / Byte,
            "MODCOD" / Array(128, Int16ul)
        )


# The dict is for ease of reading only
MODCOD: dict[int, str] = {0: "",
                          1: "",
                          2: "QPSK 1/4 NORMAL",
                          3: "QPSK 1/4 SHORT",
                          4: "QPSK 1/3 NORMAL",
                          5: "QPSK 1/3 SHORT",
                          6: "QPSK 2/5 NORMAL",
                          7: "QPSK 2/5 SHORT",
                          8: "QPSK 1/2 NORMAL",
                          9: "QPSK 1/2 SHORT",
                          10: "QPSK 3/5 NORMAL",
                          11: "QPSK 3/5 SHORT",
                          12: "QPSK 2/3 NORMAL",
                          13: "QPSK 2/3 SHORT",
                          14: "QPSK 3/4 NORMAL",
                          15: "QPSK 3/4 SHORT",
                          16: "QPSK 4/5 NORMAL",
                          17: "QPSK 4/5 SHORT",
                          18: "QPSK 5/6 NORMAL",
                          19: "QPSK 5/6 SHORT",
                          20: "QPSK 8/9 NORMAL",
                          21: "QPSK 8/9 SHORT",
                          22: "QPSK 9/10 NORMAL",
                          23: "",
                          24: "8PSK 3/5 NORMAL",
                          25: "8PSK 3/5 SHORT",
                          26: "8PSK 2/3 NORMAL",
                          27: "8PSK 2/3 SHORT",
                          28: "8PSK 3/4 NORMAL",
                          29: "8PSK 3/4 SHORT",
                          30: "8PSK 5/6 NORMAL",
                          31: "8PSK 5/6 SHORT",
                          32: "8PSK 8/9 NORMAL",
                          33: "8PSK 8/9 SHORT",
                          34: "8PSK 9/10 NORMAL",
                          35: "",
                          36: "16APSK 2/3 NORMAL",
                          37: "16APSK 2/3 SHORT",
                          38: "16APSK 3/4 NORMAL",
                          39: "16APSK 3/4 SHORT",
                          40: "16APSK 4/5 NORMAL",
                          41: "16APSK 4/5 SHORT",
                          42: "16APSK 5/6 NORMAL",
                          43: "16APSK 5/6 SHORT",
                          44: "16APSK 8/9 NORMAL",
                          45: "16APSK 8/9 SHORT",
                          46: "16APSK 9/10 NORMAL",
                          47: "",
                          48: "32APSK 3/4 NORMAL",
                          49: "32APSK 3/4 SHORT",
                          50: "32APSK 4/5 NORMAL",
                          51: "32APSK 4/5 SHORT",
                          52: "32APSK 5/6 NORMAL",
                          53: "32APSK 5/6 SHORT",
                          54: "32APSK 8/9 NORMAL",
                          55: "32APSK 8/9 SHORT",
                          56: "32APSK 9/10 NORMAL",
                          57: "",
                          58: "",
                          59: "",
                          60: "",
                          61: "",
                          62: "",
                          63: "",
                          64: "VL SNR",
                          65: "VL SNR",
                          66: "QPSK 13/45",
                          67: "QPSK 9/20",
                          68: "QPSK 11/20",
                          69: "8APSK 5/9-L",
                          70: "8APSK 26/45-L",
                          71: "8APSK 23/36",
                          72: "8APSK 25/36",
                          73: "8APSK 13/18",
                          74: "16APSK 1/2-L",
                          75: "16APSK 8/15-L",
                          76: "16APSK 5/9-L",
                          77: "16APSK 26/45",
                          78: "16APSK 3/5",
                          79: "16APSK 3/5-L",
                          80: "16APSK 28/45",
                          81: "16APSK 23/36",
                          82: "16APSK 2/3-L",
                          83: "16APSK 25/36",
                          84: "16APSK 13/18",
                          85: "16APSK 7/9",
                          86: "16APSK 77/90",
                          87: "16APSK 2/3-L",
                          88: "",
                          89: "32APSK 32/45",
                          90: "32APSK 11/15",
                          91: "32APSK 7/9",
                          92: "64APSK 32/45-L",
                          93: "64APSK 11/15",
                          94: "",
                          95: "64APSK 7/9",
                          96: "",
                          97: "64APSK 4/5",
                          98: "",
                          99: "64APSK 5/6",
                          100: "",
                          101: "",
                          102: "",
                          103: "",
                          104: "",
                          105: "",
                          106: "",
                          107: "",
                          108: "QPSK 11/45",
                          109: "QPSK 4/15",
                          110: "QPSK 14/45",
                          111: "QPSK 7/15",
                          112: "QPSK 8/15",
                          113: "QPSK 32/45",
                          114: "QPSK 7/15",
                          115: "QPSK 8/15",
                          116: "8PSK 7/15",
                          117: "8PSK 32/45",
                          118: "16APSK 7/15",
                          119: "16APSK 8/15",
                          120: "16APSK 26/45",
                          121: "16APSK 3/5",
                          122: "16APSK 32/45",
                          123: "32APSK 2/3",
                          124: "32APSK 32/45",
                          125: "",
                          126: "",
                          127: ""}