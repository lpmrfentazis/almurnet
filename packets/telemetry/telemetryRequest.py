from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Byte,
                       Int32sl,  
                       Int16ul)

    
class TelemetryRequestPacket(BasePacket):
    """
        The class describes a packet for requesting Telemetry message
        
        If more than one demodulator is installed on the server, then when connecting to port,
        the demodulator ID setting command must be executed, otherwise the server will not issue telemetry packets.
        The "Telemetry" port allows receiving parameters of the current state of the demodulator. When
        When connecting to the port, it is necessary to send a packet specifying the parameters of the port operation.
        
         If more than one demodulator is installed on the server, it is necessary to first
        packet setting the demodulator identifier, and then the packet setting the parameters of the
        port operation parameters.
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 47
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 1
    mode: Annotated[int, Ge(0), Le(1), CTypes.Byte, "0 - send by timer, 1 - send by request"] = 1
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "mode" / Byte)
    