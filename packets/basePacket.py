from __future__ import annotations
from pydantic import BaseModel
from annotated_types import Annotated, Ge, Le
from .CTypes import CTypes
from construct import (Struct, 
                       Int32sl,
                       Int16ul)


class BaseStruct(BaseModel):
    @classmethod
    def fromBytes(cls, data) -> BasePacket:
        # PyDantic black magic
        parsed = cls._format.default.parse(data)
    
        return cls(**parsed)
    
    def toBytes(self) -> bytes:
        return self._format.build(self.model_dump())
    
    _format: Struct = Struct()


class BasePacket(BaseStruct):
    """
        Base class describing ALMUR Demodulator package 
        To describe a new packet, inherit from BasePacket and override the _format field like
    """
    startMark: Annotated[int, Ge(-1234567890), Le(-1234567890), CTypes.Int_32] = -1234567890
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort]
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul
    )