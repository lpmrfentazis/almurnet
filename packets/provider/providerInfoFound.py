from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le, MinLen, MaxLen
from construct import (Struct, 
                       Int32sl,
                       Int32ul,   
                       Int16ul,
                       Bytes,
                       PaddedString
                       )

    
    
class ProviderInfoFoundPacket(BasePacket):
    """
        The class describes the packet sent by the server if demodulators are detected
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 2
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 40
    
    demodulatorID: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    PCINumber: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    demodulatorSerialNumber: Annotated[int, Ge(0), Le(CTypes.UInt_32.max), CTypes.UInt_32]
    
    _demodulatorNameLen: int = 27
    demodulatorName: Annotated[str, MinLen(_demodulatorNameLen), MaxLen(_demodulatorNameLen)]
    
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        
        "demodulatorID" / Int32ul,
        "PCINumber" / Int32ul,
        "demodulatorSerialNumber" / Int32ul,
        "demodulatorName" / PaddedString(length=_demodulatorNameLen, encoding="utf8")
        )
    