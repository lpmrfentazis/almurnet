from packets.basePacket import BasePacket
from packets.CTypes import CTypes
from annotated_types import Annotated, Ge, Le
from construct import (Struct, 
                       Int32sl,  
                       Int16ul,
                       Byte)

    
    
class ProviderInfoNotFoundPacket(BasePacket):
    """
        The class describes the packet sent by the server if no demodulators are detected
    """
    packetID: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 2
    dataLength: Annotated[int, Ge(CTypes.UShort.min), Le(CTypes.UShort.max), CTypes.UShort] = 0
    isBusy: Annotated[int, Ge(0), Le(1), CTypes.Byte] = 1
    _format: Struct = Struct(
        "startMark" / Int32sl,
        "packetID" / Int16ul,
        "dataLength" / Int16ul,
        "isBusy" / Byte)
    