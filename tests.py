import unittest
import struct
from packets import packets


class TestBasePacket(unittest.TestCase):
    def testFromBytes(self):
        bytesData = struct.pack("<iHH", -1234567890, 48, 0)       
        
        self.assertEqual(struct.unpack("<iHH", bytesData),
                         tuple(packets.BasePacket.fromBytes(bytesData).model_dump().values()))
    
    def testToBytes(self):
        bytesData = struct.pack("<iHH", -1234567890, 48, 0)       
        
        data = packets.BasePacket(startMark=-1234567890,
                                  packetID=48,
                                  dataLength=0)
        
        self.assertEqual(bytesData, data.toBytes())
        
class TestTelemetryRequestPacket(unittest.TestCase):
    def testFromBytes(self):
        bytesData = struct.pack("<iHHB", -1234567890, 2, 40, 1) 
        self.assertEqual(struct.unpack("<iHHB", bytesData),
                         tuple(packets.TelemetryRequestPacket.fromBytes(bytesData).model_dump().values()))
    
    def testToBytes(self):
        bytesData = struct.pack("<iHHB", -1234567890, 48, 0, 1)
        data = packets.TelemetryRequestPacket(startMark=-1234567890,
                                  packetID=48,
                                  dataLength=0,
                                  mode=1)
        
        encoded = data.toBytes()
        
        self.assertEqual(bytesData, encoded)
        

class TestInfoFoundPacket(unittest.TestCase):
    def testFromBytes(self):
        bytesData = struct.pack("<iHHB", -1234567890, 48, 0, 1) 
        self.assertEqual(struct.unpack("<iHHB", bytesData),
                         tuple(packets.TelemetryRequestPacket.fromBytes(bytesData).model_dump().values()))
    
    def testToBytes(self):
        bytesData = struct.pack("<iHHB", -1234567890, 48, 0, 1)
        data = packets.TelemetryRequestPacket(startMark=-1234567890,
                                  packetID=48,
                                  dataLength=0,
                                  mode=1)
        
        encoded = data.toBytes()
        
        self.assertEqual(bytesData, encoded)

if __name__ == "__main__":
    unittest.main(verbosity=2)