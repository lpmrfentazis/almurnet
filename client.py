import asyncio
import struct

from packets import TelemetryInitPacket, TelemetryPingPacket
from packets import TelemetryPacket

class TCPClient:    
    def __init__(self, host, port):
        self.host = host
        self.port = port

    async def connect(self):
        self.reader, self.writer = await asyncio.open_connection(self.host, self.port)

    async def initConnection(self):
        self.writer.write(TelemetryInitPacket.toBytes())
        await self.writer.drain()
        
    async def requestTelemetry(self):
        self.writer.write(TelemetryPingPacket.toBytes())

    async def receive_data(self):
        data = await self.reader.read(1000)
        return data

    async def close(self):
        self.writer.close()
        await self.writer.wait_closed()

async def main():
    client = TCPClient('127.0.0.1', 6002)
    await client.connect()
    await client.initConnection()

    # Пример получения ответа от сервера
    response = await client.receive_data()
    
    print("Response from server:", response)
    
    await client.requestTelemetry()
    
    # Пример получения ответа от сервера
    response = await client.receive_data()
    
    print("Response from server:", TelemetryPacket.fromBytes(response))

    await client.close()

if __name__ == "__main__":
    asyncio.run(main())