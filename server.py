import asyncio

class TCPServer:
    def __init__(self, host, port, maxClients=20):
        self.host = host
        self.port = port
        self.maxClients = maxClients
        self.semaphore = asyncio.Semaphore(maxClients)

    async def handle_client(self, reader, writer):
        data = await reader.read(100)
        message = data.decode()
        addr = writer.get_extra_info('peername')
        print(f"Received {message!r} from {addr!r}")

        print(f"Send: {message!r}")
        writer.write(data)
        await writer.drain()

        print("Closing the connection")
        writer.close()

    async def client_handler(self, reader, writer):
        async with self.semaphore:
            await self.handle_client(reader, writer)

    async def start(self):
        server = await asyncio.start_server(
            self.client_handler, self.host, self.port)

        addr = server.sockets[0].getsockname()
        print(f'Serving on {addr}')

        async with server:
            await server.serve_forever()

if __name__ == "__main__":
    server = TCPServer('0.0.0.0', 6002)
    asyncio.run(server.start())